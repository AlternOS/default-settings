#!/bin/sh
set -e

#TODO
# - Use git rev-parse HEAD for revision
# - create directory if not existing

echo 'Update Obsidian icons theme'
cd ../iconpack-obsidian
git pull
rm -rf ../default-settings/rootfs/usr/share/icons/Obsidian*
cp -r Obsidian ../default-settings/rootfs/usr/share/icons
cp -r Obsidian-Green ../default-settings/rootfs/usr/share/icons

echo 'Update Matcha theme'
cd ../matcha
git pull
rm -rf ../default-settings/rootfs/usr/share/themes/Matcha*
mkdir ../default-settings/rootfs/usr/share/themes/Matcha
cp -r src/* ../default-settings/rootfs/usr/share/themes/Matcha/

echo 'Update Lubuntu Green icons theme'
cd ../lubuntu-green
git pull
rm -rf ../default-settings/rootfs/usr/share/icons/Lubuntu-Green*
mkdir ../default-settings/rootfs/usr/share/icons/Lubuntu-Green
cp -r Lubuntu*/* ../default-settings/rootfs/usr/share/icons/Lubuntu-Green/

echo 'Update Obsidian theme'
cd ../theme-obsidian-2
git pull
rm -rf ../default-settings/rootfs/usr/share/themes/Obsidian-2*
mkdir ../default-settings/rootfs/usr/share/themes/Obsidian-2
cp -r Obsidian-2 ../default-settings/rootfs/usr/share/themes/

echo 'Update bookmarks and feeds'
cd ../default-settings
src/bin/generate_bookmarks.py
src/bin/generate_feeds_list.py
