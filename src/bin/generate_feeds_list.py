#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
#
#       Copyright 2017 (c) Julien Lavergne <gilir@ubuntu.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

# Doc: https://docs.python.org/3/library/xml.etree.elementtree.html
import xml.etree.ElementTree as ET

# For pretty xml formating
import xml.dom.minidom as DOM

try:
    import configparser as configparser
except:
    import ConfigParser as configparser

def load_inifile(path):
    inifile = None
    inifile = configparser.ConfigParser()
    inifile.optionxform = str
    try:
        inifile.read_file(open(path))
    except:
        try:
            inifile.readfp(open(path))
        except:
            print("load_inifile: error, when loading %s as a ini file" % path)
    return inifile

def get_elem_text(text, elem):
    elem_list = list(elem)
    return_elem = None
    for i in elem_list:
        if (i.get("text") == text):
            return_elem = i

    return return_elem

def create_elem(elem, text, htmlUrl, xmlUrl):
    if (htmlUrl is None or xmlUrl is None):
        element_new = ET.Element("outline", text=text)
    else:
        element_new = ET.Element("outline", text=text, htmlUrl=htmlUrl, xmlUrl=xmlUrl)
    elem.append(element_new)
    return element_new

def write_xml(element, path):
    tree = ET.ElementTree(element)
    tree.write(path)

    xml = DOM.parse(path)
    text_file = open(path, "w")
    text_file.write(xml.toprettyxml())
    text_file.close()

def debug_ini(ini):
    for ini_item in ini.sections():
        for ini_setting in ini.options(ini_item):
            print(ini.get(ini_item, ini_setting))

def load_ini(path):

    ini = load_inifile(path)

    # Structure :
    # 1: Add <opml version="1.0">
    # 2: Add <head> <title>
    # 3: Add <outline> on 3 levels (Top category, category and title of the feed)
    # 4: Attributes : text (Title), htmlUrl (website URL), xmlUrl (RSS / XML feed)

    opml = ET.Element('opml', version="1.0")

    head = ET.SubElement(opml, 'head')
    ET.SubElement(head, 'title', text='Liferea')

    body = ET.SubElement(opml, 'body')

    for ini_item in ini.sections():
        text = ini.get(ini_item, 'text')
        htmlUrl = ini.get(ini_item, 'htmlUrl')
        if (ini.has_option(ini_item, 'xmlUrl')):
            xmlUrl = ini.get(ini_item, 'xmlUrl')
            if (text is not None):
                if (ini.get(ini_item, 'xmlUrl') is not None):
                    category = ini.get(ini_item, 'category')
                    if (category is not None):
                        category_elem = get_elem_text(category, body)
                        if (category_elem is None):
                            category_elem = create_elem(body, category, None, None)
                        if ini.has_option(ini_item, 'theme'):
                            theme = ini.get(ini_item, 'theme')
                            if (theme is not None):
                                theme_elem = get_elem_text(theme, category_elem)
                                if (theme_elem is None):
                                    theme_elem = create_elem(category_elem, theme, None, None)
                                create_elem(theme_elem, text, htmlUrl, xmlUrl)
                            else:
                                create_elem(category_elem, text, htmlUrl, xmlUrl)
                        else:
                            create_elem(category_elem, text, htmlUrl, xmlUrl)

    append_liferea_folders(body)

    return opml

def append_liferea_folders(elem):

    # TODO translate folders
    unread_folder = ET.Element("outline", text="Non lus", title="Non lus", description="Non lus", type="vfolder", htmlUrl="", xmlUrl="vfolder")
    unread = ET.Element("outline", type="rule", text="Statut", rule="unread", value="", additive="true")
    unread_folder.append(unread)
    elem.append(unread_folder)

    important_folder = ET.Element("outline", text="Importants", title="Importants", description="Importants", type="vfolder", htmlUrl="", xmlUrl="vfolder")
    important = ET.Element("outline", type="rule", text="Flag status", rule="flagged", value="", additive="true")
    important_folder.append(important)
    elem.append(important_folder)

# TODO Sanity check for ini file

if __name__ == "__main__":

    element = load_ini('src/data/links.ini')
    write_xml(element, 'rootfs/usr/share/alternos/liferea/feedlist_fr.opml')
