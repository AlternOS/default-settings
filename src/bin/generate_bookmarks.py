#!/usr/bin/env python3
# -*- coding:UTF-8 -*-
#
#       Copyright 2017 (c) Julien Lavergne <gilir@ubuntu.com>
#
#       This program is free software; you can redistribute it and/or modify
#       it under the terms of the GNU General Public License as published by
#       the Free Software Foundation; either version 2 of the License, or
#       (at your option) any later version.
#       This program is distributed in the hope that it will be useful,
#       but WITHOUT ANY WARRANTY; without even the implied warranty of
#       MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#       GNU General Public License for more details.
#       You should have received a copy of the GNU General Public License
#       along with this program; if not, write to the Free Software
#       Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#       MA 02110-1301, USA.

# Generate to distribution.ini
#
# Format
#
#[BookmarksFolder-1]
#item.1.title=the title
#item.1.link=the web link
#item.1.description=the tooltip
#item.1.keyword=one keyword
#item.1.icon=http://example.org/favicon.png
#item.1.iconData=data:image

try:
    import configparser as configparser
except:
    import ConfigParser as configparser

import fileinput

def load_inifile(path):
    inifile = None
    inifile = configparser.ConfigParser()
    inifile.optionxform = str
    try:
        inifile.read_file(open(path))
    except:
        try:
            inifile.readfp(open(path))
        except:
            print("load_inifile: error, when loading %s as a ini file" % path)
    return inifile

def save_inifile(keyfile, path):
    file_to_save = open(path,'w')
    keyfile.write(file_to_save)
    file_to_save.close()

def generate_distribution_ini(config_file):
    keyfile = configparser.ConfigParser()
    keyfile.optionxform = str
    #Put common keys
    append_constant(keyfile)

    category_list = []
    category_dict = {}

    for section in config_file.sections():
        category = config_file.get(section, 'category')
        if category not in category_list:
            category_list.append(category)
            category_dict[category] = 1

    n = 1
    for i in category_list:
        keyfile.set('BookmarksMenu','item.'+ str(n) +'.type','folder')
        keyfile.set('BookmarksMenu','item.'+ str(n) +'.title', i)
        keyfile.set('BookmarksMenu','item.'+ str(n) +'.folderId', str(n))
        keyfile.add_section('BookmarksFolder-' + str(n))
        n = n +1

    for section in config_file.sections():
        category = config_file.get(section,'category')
        folder = 'BookmarksFolder-' + str(category_list.index(config_file.get(section,'category')) + 1)        
        keyfile.set(folder, 'item.'+str(category_dict[category])+'.title', config_file.get(section,'text'))
        keyfile.set(folder, 'item.'+str(category_dict[category])+'.link', config_file.get(section,'htmlUrl'))
        category_dict[category] = category_dict[category] + 1
    return keyfile

def append_constant(keyfile):
    keyfile.add_section('Global')
    keyfile.set('Global','id','alternos')
    keyfile.set('Global','version','1.0')
    keyfile.set('Global','about','Mozilla Firefox for AlternOS')
    keyfile.set('Global','about.en-US', 'Mozilla Firefox for AlternOS')
    keyfile.set('Global','about.fr-FR','Mozilla Firefox pour AlternOS')

    keyfile.add_section('Preferences')
    keyfile.set('Preferences','mozilla.partner.id','alternos')
    keyfile.set('Preferences','app.distributor','alternos')
    keyfile.set('Preferences','app.distributor.channel','alternos')
    keyfile.set('Preferences','browser.search.distributionID','alternos')
    keyfile.set('Preferences','extensions.pocket.enabled','false')

    keyfile.add_section('LocalizablePreferences')
    keyfile.set('LocalizablePreferences','browser.startup.homepage','https://search.lilo.org/')
    keyfile.set('LocalizablePreferences','startup.homepage_welcome_url','http://alternos.frama.io/site/')

    keyfile.add_section('BookmarksMenu')

def fix_ini(path):
    import string,  os, sys

    f = open(path, 'r')
    file_contents = f.readlines()
    f.close()

    f = open(path, 'w')
    text1 = (" = ")
    text2 = ("=")

    for line in file_contents:
            f.write(line.replace(text1, text2))
    f.close()

if __name__ == "__main__":

    data = load_inifile('src/data/links.ini')
    keyfile = generate_distribution_ini(data)
    save_inifile(keyfile, 'rootfs/usr/share/alternos/firefox/distribution.ini')
    fix_ini('rootfs/usr/share/alternos/firefox/distribution.ini')
    
